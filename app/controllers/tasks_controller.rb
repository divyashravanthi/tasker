class TasksController < ApplicationController
	before_action :authenticate_user!

	def dashboard
	end

	def create
		current_user.tasks.create(title: params[:title])
		redirect_to root_path
	end

	def update_task_status
		Task.find(params[:id].to_i).toggle! :finished
		redirect_to root_path
	end

	def all_status_update
		current_user.tasks.update_all(finished: true)
		redirect_to root_path
	end
end
