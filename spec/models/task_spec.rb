require "rails_helper"

RSpec.describe Task, :type => :model do
	
	it "creates a task" do
		first_task = Task.create!(title: "Buy Shoes")

		expect(Task.first.title).to eq(first_task.title)
	end

  
end