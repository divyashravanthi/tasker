== README

This README would normally document whatever steps are necessary to get the
application up and running.

1. Clone the application.

2. Bundle gems.

3. Set the database params to the credentials required in database.yml

4. Set up the database(create and migrate)

5. Run the server.

6. It is required to authenticate before use, so once the root path is hit it will redirect to sessions page.

7. Once authenticated, enjoy adding and completing tasks. 