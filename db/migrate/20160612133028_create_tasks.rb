class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
    	t.string :title, null: false, default: ""
    	t.boolean :finished, default: false
    	t.references :user, index: true
    end
  end
end
